﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace safari
{
    public partial class main : System.Web.UI.Page
    {
        SqlDataAdapter dacx;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Hamza\source\repos\safari\safari\App_Data\Database1.mdf;Integrated Security=True");
            dacx = new SqlDataAdapter("select * from admin", con);

            if (Cache["ds"] == null)
            {
                ds = new DataSet();
                Cache["ds"] = ds;
            }
            else
            {
                ds = (DataSet)Cache["ds"];
            }

            if (ds.Tables["admin_table"] == null)
            {
                dacx.Fill(ds, "admin_table");
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bool exsit = false;
            int i = 0;
            while ((i < ds.Tables["admin_table"].Rows.Count) && (exsit == false))
            {
                if (TextBox1.Text == ds.Tables["admin_table"].Rows[i][1].ToString() && TextBox3.Text == ds.Tables["admin_table"].Rows[i][2].ToString())
                {
                    exsit = true;
                    Session["idf"] = ds.Tables["admin_table"].Rows[i][0];
                    Session["uv"] = ds.Tables["admin_table"].Rows[i][1];
                    Session["passf"] = ds.Tables["admin_table"].Rows[i][2];

                }
                else
                {
                    i++;
                }
            }

            if (exsit == true)
            {
                Response.Redirect("admin/accueil.aspx");
            }
            else
            {
                Response.Write("<script>alert('password ou num incrrect')</script>");
            }
        }
    }
}