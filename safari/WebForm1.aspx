﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="safari.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="style/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrap">
            <header>
                <div >
                    <nav id="nav_1">
                        <ul>
                            <li><%= link1 %></li>
                            <li><%= link2 %></li>
                            <li><%= link3 %></li>
                            <li><%= link4 %></li>
                            <li><%= link5 %></li>
                            <li><%= link6 %></li>
                        </ul>
                    </nav>
                </div>
                <div id="zone_2">
                    <div id="logo">
                        <span></span>
                        <h3>Safari Adventure</h3>
                    </div>
                    <div id="search">
                        <img src="images/search_img.png" width="25px" height="25px" />
                    </div>
                </div>
            </header>
            <selection>
                    <div id="slider">
                        <img src="images/<%= picture_name %>" width="100%" />
                        <h1><%= title_slider %></h1>
                        <h3 id="small_title"><%= sub_title_slider %></h3>
                    </div>
                <a href="https://localhost:44302/inscription.aspx?id=<%= id1 %>">
                    <div id="card_wrap">
                        <div class="card_1">
                            <div class="headline">
                                <h4><%=btitle1 %></h4>
                            </div>
                             <div class="img_card">
                                <img src="images/<%= b_picture1 %>"  width="100%" height="170px"/>
                            </div>
                            <article>
                                <h5><%= b_subtitle1 %></h5>
                                <p><%= b_desc1 %></p>
                            </article>
                            <div id="button">
                                <button>More</button>
                            </div>
                        </div>
                        </a>
                        <a href="https://localhost:44302/inscription.aspx?id=<%= id2 %>">
                       <div class="card_1">
                           
                            <div class="headline">
                                <h4><%=btitle2 %></h4>
                            </div>
                             <div class="img_card">
                                <img src="images/<%= b_picture2 %>"  width="100%" height="170px"/>
                            </div>
                            <article>
                                <h5><%= b_subtitle2 %></h5>
                                <p><%= b_desc2 %></p>
                            </article>
                            <div id="button">
                                <button>More</button>
                            </div>
                              
                        </div>
                         </a>
                     <a href="https://localhost:44302/inscription.aspx?id=<%= id3 %>">
                     <div class="card_1">
                         
                            <div class="headline">
                                <h4><%=btitle3 %></h4>
                            </div>
                             <div class="img_card">
                                <img src="images/<%= b_picture3 %>"  width="100%" height="170px"/>
                            </div>
                            <article>
                                <h5><%= b_subtitle3 %></h5>
                                <p><%= b_desc3 %></p>
                            </article>
                            <div id="button">
                               <button>More</button>
                            </div>
                        </div>
                        </a>
                    </div>
            </section>
    </form>
</body>
</html>
