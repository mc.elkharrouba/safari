﻿//la fonction validate, vait usage d'un
//objet de type etudiant afin d'effectuer la
//validation du formulaire index.html
function validate() {
    var nom = document.getElementById('nom');
    var text = document.getElementById('text');

    var email = document.getElementById('email');
    var tel = document.getElementById('tel');
  

    var e = new etudiant(nom.value, text.value, email.value, tel.value);

    if (!e.validateNom()) {
        nom.style.borderColor = "red";

    }
    else {
        nom.style.borderColor = "black";
    }

    if (!e.validateText()) {
        text.style.borderColor = "red";

    }
    else {
        pretextnom.style.borderColor = "black";
    }

 

    if (!e.validateTel()) {
        tel.style.borderColor = "red";

    }
    else {
        tel.style.borderColor = "black";
    }

    if (!e.validateEmail()) {
        email.style.borderColor = "red";

    }
    else {
        email.style.borderColor = "black";
    }

}
//création du constructeur de la classe étudiant
function etudiant(nom, text, email, tel) {
    this.nom = nom;
    this.text = text;
    this.email = email;
    this.tel = tel;


}
//propriétée de la classe étudiant qui
//permet de réccupérer le nom
etudiant.prototype.getNom = function () {
    return this.nom;
}
//propriétée de la classe étudiant qui
//permet de modifier le nom
etudiant.prototype.setNom = function (nom) {
    this.nom = nom;
}
//propriétée de la classe étudiant qui
//permet de réccupérer le prénom
etudiant.prototype.getText = function () {
    return this.text;
}
//propriétée de la classe étudiant qui
//permet de modifier le prénom
etudiant.prototype.setText = function (text) {
    this.text = text;
}

//propriétée de la classe étudiant qui
//permet de réccupérer le mail
etudiant.prototype.getMail = function () {
    return this.email;
}
//propriétée de la classe étudiant qui
//permet de modifier le mail
etudiant.prototype.setMail = function (email) {
    this.email = email;

}

//propriétée de la classe étudiant qui
//permet de modifier le tel
etudiant.prototype.setTel = function (tel) {

    this.tel = tel;

}
//propriétée de la classe étudiant qui
//permet de réccupérer le tel
etudiant.prototype.getTel = function () {
    return this.tel;
}

//fonction permettant de valider le nom
etudiant.prototype.validateNom = function () {
    var status;

    for (var i = 0; i < this.nom.length; i++) {
        if (((this.nom.charCodeAt(i) >= "A".charCodeAt(0)) && (this.nom.charCodeAt(i) <= "Z".charCodeAt(0))) ||
            ((this.nom.charCodeAt(i) >= "a".charCodeAt(0)) && (this.nom.charCodeAt(i) <= "z".charCodeAt(0)))) {
            status = true;

        }
        else {
            status = false;
        }
    }

    return status;


}
//fonction permettant de valider le text
etudiant.prototype.validateText = function () {
    var status;

    for (var i = 0; i < this.text.length; i++) {
        if (((this.text.charCodeAt(i) >= "A".charCodeAt(0)) && (this.text.charCodeAt(i) <= "Z".charCodeAt(0))) ||
            ((this.text.charCodeAt(i) >= "a".charCodeAt(0)) && (this.text.charCodeAt(i) <= "z".charCodeAt(0)))) {
            status = true;

        }
        else {
            status = false;
        }
    }

    return status;
}

//fonction permettant de valider le numéro de tel
etudiant.prototype.validateTel = function () {

    var telRegEx = /^(\(\d{3}\)) ?\d{3}-\d{4}/;
    if (telRegEx.test(this.tel)) {
        return true;  //code
    }
    else {
        return false;
    }


}
//fonction permettant de valider le email
etudiant.prototype.validateEmail = function () {

    var emailRegEx = /[a-zA-Z0-9]+(\.|_|-)?[a-zA-Z0-9]+@teccart\.qc\.ca/;
    if (emailRegEx.test(this.email)) {
        return true;  //code
    }
    else {
        return false;
    }


}