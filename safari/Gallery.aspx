﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="safari.WebForm2" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="style/style.css" rel="stylesheet" />
    <script>
        //function openBox() {
        //    document.getElementById("mymodel").style.display = "block";

         
          
        //        var b = document.getElementById("myimagenames").getAttribute("src");

        //        alert(b);
            
        //}

        // Close the Modal
        function closeModal() {
            document.getElementById("mymodel").style.display = "none";
        }
    
    </script>
</head>
<body>
    <form id="form2" runat="server">
       <div id="wrap">
                <header>
                    <div >
                        <nav id="nav_1">
                            <ul>
                                <li>Home</li>
                                <li>About</li>
                                <li>Pages</li>
                                <li>Gallery</li>
                                <li>Blog</li>
                                <li>Contact</li>
                            </ul>
                        </nav>
                    </div>
                    <div id="zone_2">
                        <div id="logo">
                            <span></span>
                            <h3>Safari Adventure</h3>
                        </div>
                        <div id="search">
                            <img src="images/search_img.png" width="25px" height="25px" />
                        </div>
                    </div>
                </header>
                <div id="section">
                           <div id="title_section">
                                <h2>Gallery</h2>
                           </div>
                            <div id="card_picture">
                                 
                                    <div class="picture_gallery">
                                        <a href ="images/<%=links_photo1%>" ><img src="images/<%=links_photo1%>" title="" width="210px" height="210px;" /></a>
                                    </div>
                                    <div class="picture_gallery">
                                        <a href ="images/<%=links_photo2%>" ><img src="images/<%=links_photo2%>" title="" width="210px" height="210px;" /></a>
                                    </div>
                                    <div class="picture_gallery">
                                        <a href ="images/<%=links_photo3%>" ><img src="images/<%=links_photo3%>" title="" width="210px" height="210px;" /></a>
                                        <asp:Button ID="Button1" runat="server" Text="Button" />
                                    </div>
                                    <div class="picture_gallery">
                                        <a href ="images/<%=links_photo4%>" ><img src="images/<%=links_photo4%>" title="" width="210px" height="210px;" /></a>
                                    </div>
                                    <div class="picture_gallery">
                                        <a href ="images/<%=links_photo5%>" ><img src="images/<%=links_photo5%>" title="" width="210px" height="210px;" /></a>
                                    </div>
                                    <div class="picture_gallery">
                                        <a href ="images/<%=links_photo6%>" ><img src="images/<%=links_photo6%>" title="" width="210px" height="210px;" /></a>
                                    </div>

                                 
                            </div>
                        
             </div>
      
    </form>
</body>
</html>
