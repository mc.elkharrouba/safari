﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inscription.aspx.cs" Inherits="safari.WebForm20" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    
    <link href="style/style.css" rel="stylesheet" />
    <script src="script/code.js"></script>
</head>
<body>
    <form id="form19" runat="server">
        <div id="wrap">
            <header>
                <div >
                    <nav id="nav_1">
                        <ul>
                            <li><%= link1 %></li>
                            <li><%= link2 %></li>
                            <li><%= link3 %></li>
                            <li><%= link4 %></li>
                            <li><%= link5 %></li>
                            <li><%= link6 %></li>
                        </ul>
                    </nav>
                </div>
                <div id="zone_2">
                    <div id="logo">
                        <span></span>
                        <h3>Safari Adventure</h3>
                    </div>
                    <div id="search">
                        <img src="images/search_img.png" width="25px" height="25px" />
                    </div>
                </div>
            </header>
            <section style="background-color:aliceblue; width:100%; height:550px;" >
               
                <h5 id="h5-insc"><%=btitle1 %></h5>

                <img id="img-insc" src="images/<%=b_picture1 %>" width="450px" height="450px" />
               
                <h6 id="h5-insc"><%=b_subtitle1 %></h6> 
                <p id="p-insc"><%=b_desc1 %></p>
                <p id="p-insc">Price : <span><%=price %></span>$</p>
                <%--<input  type="text" placeholder="Name" id="nom" class="auto-style1" /> --%>
                <asp:TextBox ID="txtnom" runat="server" SkinID="email" Width="220px">Nom</asp:TextBox>
                <br />
                <asp:TextBox ID="txtprenom" runat="server" SkinID="email" Width="220px" >Prenom</asp:TextBox>
                <br />
                <asp:TextBox ID="Txtemail" runat="server" SkinID="email" Width="220px" >Email</asp:TextBox>
                <br />
                <asp:TextBox ID="txttel" runat="server" SkinID="tel" Width="220px">Telephone</asp:TextBox>
                <%--<input type="phone" placeholder="Phone"  id="tel" class="auto-style1" />--%>
                <br />

                  <asp:Button ID="Button1" runat="server" Text="Inscription" CssClass="btn"  style="text-transform: uppercase" OnClick="Button1_Click"  />
        <%--        <input type="button" class="btn" value="inscription" onclick="validate();" /> --%>



            </section>
        </div>
    </form>
</body>
</html>
