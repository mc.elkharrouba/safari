﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contactus.aspx.cs" Inherits="safari.contactus" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="style/style.css" rel="stylesheet" />
    <script src="script/code.js"></script>
    <style type="text/css">
        .auto-style1 {
            text-transform: uppercase;
        }
    </style>
</head>
<body>
    <form id="form3" runat="server">
       <div id="wrap">
                <header>
                    <div >
                        <nav id="nav_1">
                            <ul>
                                <li>Home</li>
                                <li>About</li>
                                <li>Pages</li>
                                <li>Gallery</li>
                                <li>Blog</li>
                                <li>Contact</li>
                            </ul>
                        </nav>
                    </div>
                    <div id="zone_2">
                        <div id="logo">
                            <span></span>
                            <h3>Safari Adventure</h3>
                        </div>
                        <div id="search">
                            <img src="images/search_img.png" width="25px" height="25px" />
                        </div>
                    </div>
                </header>
                <div id="section">
                      
                    <div id="side_left">
                        <h3><%= header_adresse %></h3>
                        <p><span></span><%=adresse %></p>
                        <h3><%=header_phone %></h3>
                        <p><span></span><%=phone1 %></p>
                        <p><span></span><%=phone2 %></p>
                        <h3><%=header_mail  %></h3>
                        <p><span></span><%=information_mail %></p>
                        <p><%=mail %></p>
                    </div>
                    <div id="side_right">
                        <h3>MISELLANEOUS INFORMATION:</h3>
                        <p>Email us any question or inquiries or use our contact data.</p>
                        <asp:TextBox ID="TxtTo" runat="server" SkinID="email" >to</asp:TextBox>
                        <asp:TextBox ID="txtSubject" runat="server" SkinID="nom">subject</asp:TextBox>
                        <asp:TextBox ID="txtMessage" runat="server" SkinID="nom" Height="244px" Width="474px">message</asp:TextBox>
                        
                        <input  type="text" placeholder="Name" id="nom" class="auto-style1" />
                       <!-- <input type="email" placeholder="Email" id="email" class="auto-style1" /> -->
                   
                        <!-- <input type="phone" placeholder="Phone"  id="tel" class="auto-style1" /> -->
                        
                        
                         <!-- <textarea cols="num" rows="num" placeholder="Message" id="text" class="auto-style1"></textarea> -->
                        <br class="auto-style1" />
                        
                        <asp:Button ID="Button1" runat="server" Text="send" CssClass="btn"  style="text-transform: uppercase" OnClick="Button1_Click1" />
                        
                        <span class="auto-style1">
                        
                        <!--<input type="button" class="btn" value="send" onclick="validate();" /> -->
                        </span>
                        <input type="button" class="btn" value="clear" style="text-transform: uppercase" />

                    </div>
                </div>       
           </div>
    </form>
</body>
</html>
