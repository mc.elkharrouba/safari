﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="listselection.aspx.cs" Inherits="safari.admin.listselection" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource2" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                <asp:BoundField DataField="prix" HeaderText="prix" SortExpression="prix" />
                <asp:BoundField DataField="sub_title" HeaderText="sub_title" SortExpression="sub_title" />
                <asp:BoundField DataField="defaultPage" HeaderText="defaultPage" SortExpression="defaultPage" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Id], [title], [prix], [sub_title], [defaultPage] FROM [blocks]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <div>
        </div>
    </form>
</body>
</html>
