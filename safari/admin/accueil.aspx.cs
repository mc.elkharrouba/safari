﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace safari.admin
{
    public partial class accueil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["uv"] == null) 
            {
                Response.Redirect("../main.aspx");
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("addvoaygae.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("updateVoyage.aspx");
        }
    }
}